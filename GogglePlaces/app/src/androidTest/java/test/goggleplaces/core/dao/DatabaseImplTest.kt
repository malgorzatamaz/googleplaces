package test.goggleplaces.core.dao

import android.support.test.InstrumentationRegistry
import org.junit.After
import org.junit.Before
import org.junit.Test

import java.util.concurrent.CountDownLatch

import io.realm.Realm
import io.realm.RealmConfiguration

import test.goggleplaces.core.model.Place

class DatabaseImplTest {
    lateinit var database: Database
    private var doneSignal: CountDownLatch? = null
    private var addedSignal: CountDownLatch? = null
    private var deletedSignal: CountDownLatch? = null
    internal var id = ""

    @Before
    @Throws(Exception::class)
    fun setUp() {
        val appContext = InstrumentationRegistry.getTargetContext()

        Realm.init(appContext)
        val realmConfig = RealmConfiguration.Builder()
                .name("placesList.realm")
                .schemaVersion(0)
                .build()
        Realm.setDefaultConfiguration(realmConfig)


        database = DatabaseImpl()
    }


    @Test
    fun deletePlace() {
        doneSignal = CountDownLatch(1)
        addedSignal = CountDownLatch(1)

        database.addNewPlace("Place1", "Fiołkowa", 49.1, 19.2, object : PlaceAddedListener {
            override fun onAdded(place: Place) {
                id = place.id
                addedSignal!!.countDown()
            }

            override fun onError(error: Throwable) {
                throw error
            }
        })

        try {
            addedSignal!!.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

        database.deletePlaceById(id, object : PlaceDeletedListener{
            override fun onDeleted() {
                doneSignal!!.countDown()
            }

            override fun onError(error: Throwable) {
                throw error
            }
        })

        try {
            doneSignal!!.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }

    @Test
    fun addPlace() {
        doneSignal = CountDownLatch(1)

        database.addNewPlace("Place1", "Fiołkowa", 49.1, 19.2, object : PlaceAddedListener {
            override fun onAdded(place: Place) {
                assert(place.name == "Place1")
                doneSignal!!.countDown()
            }

            override fun onError(error: Throwable) {
                throw error
            }

        })

        try {
            doneSignal!!.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

    }

    @Test
    fun getPlaces() {
        doneSignal = CountDownLatch(1)
        addedSignal = CountDownLatch(3)

        database.clear()

        database.addNewPlace("Place1", "Address1", 49.1, 19.2, object : PlaceAddedListener {
            override fun onAdded(place: Place) {
                addedSignal!!.countDown()
            }

            override fun onError(e: Throwable) {
                throw Error("Error while adding Place1")
            }
        })

        database.addNewPlace("Place2", "Address2", 49.1, 19.2, object : PlaceAddedListener {
            override fun onAdded(place: Place) {
                addedSignal!!.countDown()
            }


            override fun onError(error: Throwable) {
                throw error
            }
        })


        database.addNewPlace("Place3", "Address3", 49.1, 19.2, object : PlaceAddedListener {
            override fun onAdded(place: Place) {
                addedSignal!!.countDown()
            }

            override fun onError(error: Throwable) {
                throw error
            }
        })

        try {
            addedSignal!!.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

        database.getAllPlaces(object : PlacesLoadedListener {
            override fun onPlacesLoaded(places: List<Place>) {
                assert(places.size == 3)
                doneSignal!!.countDown()
            }

            override fun onError(e: Throwable) {
                throw Error("Error while getting all placesList")
            }
        })

        try {
            doneSignal!!.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }

    @Test
    fun deleteAllPlaces() {
        var places = ArrayList<Place>()
        doneSignal = CountDownLatch(1)
        deletedSignal = CountDownLatch(1)
        addedSignal = CountDownLatch(3)

        database.clear()

        database.addNewPlace("Place1", "Address1", 49.1, 19.2, object : PlaceAddedListener {
            override fun onAdded(place: Place) {
                places.add(place)
                addedSignal!!.countDown()
            }

            override fun onError(e: Throwable) {
                throw Error("Error while adding Place1")
            }
        })

        database.addNewPlace("Place2", "Address2", 49.1, 19.2, object : PlaceAddedListener {
            override fun onAdded(place: Place) {
                places.add(place)
                addedSignal!!.countDown()
            }


            override fun onError(error: Throwable) {
                throw error
            }
        })


        database.addNewPlace("Place3", "Address3", 49.1, 19.2, object : PlaceAddedListener {
            override fun onAdded(place: Place) {
                addedSignal!!.countDown()
                places.add(place)
            }

            override fun onError(error: Throwable) {
                throw error
            }
        })

        try {
            addedSignal!!.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

        database.deletePlaces(places, object : PlaceDeletedListener{
            override fun onDeleted() {
                deletedSignal!!.countDown()
            }

            override fun onError(error: Throwable) {
                throw error
            }
        })

        try {
            deletedSignal!!.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

        database.getAllPlaces(object : PlacesLoadedListener {
            override fun onPlacesLoaded(places: List<Place>) {
                assert(places.isEmpty())
                doneSignal!!.countDown()
            }

            override fun onError(e: Throwable) {
                throw Error("Error while getting all placesList")
            }
        })

        try {
            doneSignal!!.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }

    @Test
    fun getPlace() {
        doneSignal = CountDownLatch(1)
        addedSignal = CountDownLatch(1)

        database.addNewPlace("Place1", "Address1", 49.1, 19.2, object : PlaceAddedListener {
            override fun onAdded(place: Place) {
                id = place.id
                addedSignal!!.countDown()
            }

            override fun onError(error: Throwable) {
                throw error
            }
        })

        try {
            addedSignal!!.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

        database.getPlaceById(id, object : PlacesLoadedListener {
            override fun onPlacesLoaded(places: List<Place>) {
                assert(places[0].name == "Place1" && places[0].address == "Address1")
                doneSignal!!.countDown()
            }

            override fun onError(error: Throwable) {
                throw error
            }
        })


        try {
            doneSignal!!.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        database.destroy()
    }
}