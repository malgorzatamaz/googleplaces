package test.goggleplaces.core.places

import android.support.test.InstrumentationRegistry
import org.junit.Test

import org.junit.Before
import test.goggleplaces.core.model.Place
import test.goggleplaces.core.places.listeners.DirectionsLoadedListener
import test.goggleplaces.core.places.listeners.FindPlaceListener
import test.goggleplaces.core.places.listeners.OnAutocompleteHintListener
import test.goggleplaces.core.places.model.DirectionsResult
import test.goggleplaces.core.places.model.Prediction
import java.util.concurrent.CountDownLatch

class PlacesServiceImplTest {

    private lateinit var service: PlacesServiceImpl
    private lateinit var doneSignal: CountDownLatch

    @Before
    @Throws(Exception::class)
    fun setUp() {
        val appContext = InstrumentationRegistry.getTargetContext()
        service = PlacesServiceImpl(appContext)
    }


    @Test
    fun getAutocompleteResult() {
        doneSignal = CountDownLatch(1)

        service.getAutocompleteResult("Gemini", object : OnAutocompleteHintListener {
            override fun onFound(foundHints: List<Prediction>) {
                assert(foundHints.isNotEmpty())
                doneSignal.countDown()
            }

            override fun onNotFound() {
                doneSignal.countDown()
            }

            override fun onError(error: Throwable) {
                throw error
            }

        })

        try {
            doneSignal.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

    }

    @Test
    fun getDirections() {
        doneSignal = CountDownLatch(1)
        service.getDirections(49.670674, 19.236762,49.655784, 19.168793, object : DirectionsLoadedListener{
            override fun onError(error: Throwable) {
                throw error
            }

            override fun onLoaded(result: DirectionsResult) {
                assert(!result.routes?.isEmpty()!!)
                doneSignal.await()
            }
        })

        try {
            doneSignal.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }

    @Test
    fun findPlace() {
        doneSignal = CountDownLatch(1)
        service.findPlace("Warsaw,Poland", object : FindPlaceListener {
            override fun onFound(place: Place) {
                doneSignal.countDown()
            }

            override fun onNotFound() {
                throw Throwable("Place not found")
            }

            override fun onError(error: Throwable) {
                throw error
            }
        })

        try {
            doneSignal.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }
}