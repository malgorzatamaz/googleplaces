package test.goggleplaces.network

import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Query
import test.goggleplaces.core.places.model.Candidate
import test.goggleplaces.core.places.model.DirectionsResult
import test.goggleplaces.core.places.model.PlaceResult
import test.goggleplaces.core.places.model.PlacesResult

interface NetworkService {
    @GET("/maps/api/place/autocomplete/json")
    fun getPlacesAutocomplete(@Query("key") key: String, @Query("input") input: String): Observable<PlacesResult>

    @GET("/maps/api/place/findplacefromtext/json")
    fun getPlaceByName(@Query("key") key: String, @Query("input") input: String, @Query("inputtype") inputype: String): Observable<PlacesResult>

    @GET("/maps/api/place/details/json")
    fun getPlaceById(@Query("key") key: String, @Query("placeid") id: String): Observable<PlaceResult>

    @GET("/maps/api/directions/json")
    fun getDirections(@Query("key") key: String, @Query("origin") origin: String, @Query("destination") destination: String): Observable<DirectionsResult>
}