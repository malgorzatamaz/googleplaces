package test.goggleplaces.application

import android.app.Application
import io.realm.Realm
import test.goggleplaces.di.AppModule
import test.goggleplaces.di.DaggerMainComponent
import test.goggleplaces.di.MainComponent
import io.realm.RealmConfiguration
import test.goggleplaces.core.activities.MainActivity
import test.goggleplaces.core.activities.MapActivity


class PlacesApplication: Application() {
    lateinit var mainComponent: MainComponent

    private fun initDagger(app: PlacesApplication): MainComponent =
            DaggerMainComponent.builder()
                    .appModule(AppModule(app))
                    .build()


    override fun onCreate() {
        super.onCreate()
        mainComponent = initDagger(this)
        Realm.init(this)
        val realmConfig = RealmConfiguration.Builder()
                .name("placesList.realm")
                .schemaVersion(0)
                .build()
        Realm.setDefaultConfiguration(realmConfig)
    }

//    fun inject(activity: Any) {
//        if (activity is MainActivity) {
//            mainComponent.inject(activity)
//        } else if (activity is MapActivity) {
//            mainComponent.inject(activity)
//        }
//    }
}