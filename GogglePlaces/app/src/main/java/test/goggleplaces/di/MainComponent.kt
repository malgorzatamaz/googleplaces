package test.goggleplaces.di

import android.app.Activity
import dagger.Component
import test.goggleplaces.core.activities.MainActivity
import test.goggleplaces.core.activities.MapActivity
import javax.inject.Singleton


@Singleton
@Component(modules = [AppModule::class, DatabaseModule::class, PresenterModule::class,PlacesServiceModule::class])
interface MainComponent {
    fun inject(target: MainActivity)
    fun inject(target: MapActivity)
}
