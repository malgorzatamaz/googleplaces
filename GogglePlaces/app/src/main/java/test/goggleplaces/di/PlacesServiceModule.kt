package test.goggleplaces.di

import android.content.Context
import dagger.Module
import dagger.Provides
import test.goggleplaces.core.places.PlacesService
import test.goggleplaces.core.places.PlacesServiceImpl
import javax.inject.Singleton


@Module
class PlacesServiceModule {
    @Provides
    @Singleton
    fun providePlacesService(context: Context): PlacesService {
        return PlacesServiceImpl(context)
    }
}