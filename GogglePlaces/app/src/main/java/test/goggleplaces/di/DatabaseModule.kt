package test.goggleplaces.di

import dagger.Module
import dagger.Provides
import test.goggleplaces.core.dao.Database
import test.goggleplaces.core.dao.DatabaseImpl
import javax.inject.Singleton

@Module
class DatabaseModule{
    @Provides
    @Singleton
    fun provideDatabase(): Database = DatabaseImpl()
}