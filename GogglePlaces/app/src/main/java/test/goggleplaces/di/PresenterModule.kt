package test.goggleplaces.di

import dagger.Module
import dagger.Provides
import test.goggleplaces.core.dao.Database
import test.goggleplaces.core.places.PlacesService
import test.goggleplaces.core.presenters.MainPresenter
import test.goggleplaces.core.presenters.MainPresenterImpl
import test.goggleplaces.core.presenters.MapPresenter
import test.goggleplaces.core.presenters.MapPresenterImpl
import javax.inject.Singleton

@Module
class PresenterModule {
    @Provides
    @Singleton
    fun provideMainPresenter(database: Database, placesService: PlacesService): MainPresenter {
        return MainPresenterImpl(database, placesService)
    }

    @Provides
    @Singleton
    fun provideMapPresenter(placesService: PlacesService): MapPresenter {
        return MapPresenterImpl(placesService)
    }
}