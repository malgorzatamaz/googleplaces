package test.goggleplaces.core.places.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PlacesResult{
    @SerializedName("status")
    var status =""

    @SerializedName("predictions")
    @Expose
    var predictions: List<Prediction>? = null


    @SerializedName("candidates")
    @Expose
    var candidates: List<Candidate>? = null
}