package test.goggleplaces.core.dao

import test.goggleplaces.core.model.Place

interface PlacesLoadedListener{
    fun onPlacesLoaded(places: List<Place>)
    fun onError(error: Throwable)
}