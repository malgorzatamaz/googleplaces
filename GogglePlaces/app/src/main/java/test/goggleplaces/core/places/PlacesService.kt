package test.goggleplaces.core.places

import test.goggleplaces.core.places.listeners.DirectionsLoadedListener
import test.goggleplaces.core.places.listeners.FindPlaceListener
import test.goggleplaces.core.places.listeners.OnAutocompleteHintListener

interface PlacesService{
    fun getAutocompleteResult(value: String, listener: OnAutocompleteHintListener)
    fun findPlace(id: String, listener: FindPlaceListener)
    fun getDirections(lat: Double, lng: Double, lat2: Double, lng2: Double, directionsLoadedListener: DirectionsLoadedListener)
    fun getPlaceById(placeId: String, listener: FindPlaceListener)
}