package test.goggleplaces.core.activities

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.Menu
import test.goggleplaces.R
import android.view.MenuItem
import test.goggleplaces.application.PlacesApplication
import test.goggleplaces.core.adapters.PlacesListAdapter
import test.goggleplaces.core.model.Place
import test.goggleplaces.core.presenters.MainPresenter
import test.goggleplaces.core.presenters.MainView
import java.lang.ref.WeakReference
import javax.inject.Inject

import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.AdapterView
import android.widget.ListView
import kotlinx.android.synthetic.main.activity_main.*
import android.graphics.PorterDuff
import android.text.Editable
import android.text.TextWatcher
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.WindowManager
import test.goggleplaces.core.adapters.PlacesAutocompleteAdapter
import test.goggleplaces.core.places.model.Prediction
import android.app.Activity
import android.view.inputmethod.InputMethodManager


class MainActivity : AppCompatActivity(), MainView {
    private var mapMenuItem: MenuItem? = null
    private var navigateMenuItem: MenuItem? = null
    private var deleteMenuItem: MenuItem? = null
    private var placesListAdapter: PlacesListAdapter? = null
    private var placesAutocompleteAdapter: PlacesAutocompleteAdapter? = null

    @Inject
    lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        (application as PlacesApplication).mainComponent.inject(this)
        presenter.setMainView(WeakReference(this))

        initAutocomplete()
        initList()
    }

    override fun onHintLoaded(places: List<Prediction>) {
        if (placesAutocompleteAdapter == null) {
            placesAutocompleteAdapter = PlacesAutocompleteAdapter(this, places)
            places_tv.setAdapter(placesAutocompleteAdapter)
            //placesAutocompleteAdapter!!.updateData(places)

        } else {
            placesAutocompleteAdapter!!.updateData(places)
        }

        if (!places.isEmpty()) {
            places_tv.setBackgroundColor(ContextCompat.getColor(getContext(), android.R.color.transparent))
        }

    }

    override fun disableAddButton() {
        add_place.isEnabled = false
    }

    override fun enableAddButton() {
        add_place.isEnabled = true
    }

    private fun initList() {
        places_lv.onItemClickListener = AdapterView.OnItemClickListener { adapter, view, pos, p3 ->
            val place = adapter.getItemAtPosition(pos) as Place
            if (presenter.changeSelection(place))
                view.setBackgroundColor(ContextCompat.getColor(this, R.color.highlighted))
            else
                view.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white))
        }

        presenter.getSavedPlaces()
    }

    override fun onSelectionCountChanged(selectedCount: Int) {
        hideMenu()

        if (selectedCount > 0) {
            deleteMenuItem?.isVisible = true
        }

        if (selectedCount == 1) {
            mapMenuItem?.isVisible = true
        }

        if (selectedCount == 2) {
            navigateMenuItem?.isVisible = true
        }
    }

    private fun hideMenu() {
        mapMenuItem?.isVisible = false
        navigateMenuItem?.isVisible = false
        deleteMenuItem?.isVisible = false
    }

    override fun getContext(): Context {
        return this
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu, menu)

        mapMenuItem = menu.findItem(R.id.map)
        navigateMenuItem = menu.findItem(R.id.navigate)
        deleteMenuItem = menu.findItem(R.id.delete)

        hideMenu()

        setWhiteColor(mapMenuItem)
        setWhiteColor(navigateMenuItem)
        setWhiteColor(deleteMenuItem)

        return true
    }

    private fun setWhiteColor(menuItem: MenuItem?) {
        if (menuItem != null) {
            val drawable = menuItem.icon
            if (drawable != null) {
                drawable.mutate()
                drawable.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.map -> {
                presenter.showOnMap()
                true
            }
            R.id.navigate -> {
                presenter.showRoute()
                true
            }
            R.id.delete -> {
                presenter.deleteSelected()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDeleteEnabled() {
        if (deleteMenuItem != null)
            deleteMenuItem!!.isVisible = true
    }

    override fun onNavigateEnabled() {
        if (navigateMenuItem != null)
            navigateMenuItem!!.isVisible = true
    }

    override fun onShowOnMapEnabled() {
        if (navigateMenuItem != null)
            navigateMenuItem!!.isVisible = true
    }


    override fun onSavedPlacesLoaded(places: List<Place>) {
        if (placesListAdapter == null) {
            places_lv.choiceMode = ListView.CHOICE_MODE_MULTIPLE
            places_lv.itemsCanFocus = false
            placesListAdapter = PlacesListAdapter(this, places)
            places_lv.adapter = placesListAdapter
        } else {
            placesListAdapter!!.updateData(places)
        }

        if (places.isEmpty())
            hideMenu()
    }

    override fun onPlaceDeleteError() {
        showDialog(getString(R.string.places_alert_title),
                getString(R.string.delete_places_alert_message))
    }

    override fun onPlacesLoadError() {
        showDialog(getString(R.string.places_alert_title),
                getString(R.string.saved_places_alert_message))
    }


    override fun onPlaceAddError() {
        showDialog(getString(R.string.places_alert_title),
                getString(R.string.add_place_alert_message))
    }

    private fun showDialog(title: String, message: String) {
        val alertDialog = AlertDialog.Builder(this).create()
        alertDialog.setTitle(title)
        alertDialog.setMessage(message)
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK"
        ) { dialog, which -> dialog.dismiss() }
        alertDialog.show()
    }

    fun onAddPlaceClicked(view: View) {
        presenter.addNewPlace()

        places_tv.setText("")
        places_tv.clearFocus()
        presenter.hideKeyboard(this)
    }


    private fun initAutocomplete() {
        add_place.isEnabled = false

        places_tv.onItemClickListener = AdapterView.OnItemClickListener { adapter, p1, pos, p3 ->
            val place = adapter!!.getItemAtPosition(pos) as Prediction
            presenter.searchPlace(place.description!!)

            places_tv.setText(place.description)
            places_tv.setSelection(place.description!!.length)
        }

        places_tv.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(value: CharSequence, start: Int, before: Int, count: Int) {
                if (value.length > 3) {
                    presenter.getPlacesHint(value)
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun afterTextChanged(s: Editable) {

            }
        })
    }

    override fun onPlaceNotFound() {
        places_tv.setBackgroundColor(ContextCompat.getColor(this, R.color.error_highlight))
        hidePlaceSearchLoader()
    }

    override fun onShowSelectedMarker(name: String, latitude: Double?, longitude: Double?) {
        val intent = Intent(this, MapActivity::class.java)
        intent.putExtra("name", name)
        intent.putExtra("latitude", latitude)
        intent.putExtra("longitude", longitude)
        startActivity(intent)
    }

    override fun onShowRoute(selected: List<Place>) {
        if(selected.size == 2) {
            val intent = Intent(this, MapActivity::class.java)
            intent.putExtra("name1", selected[0].name)
            intent.putExtra("latitude1", selected[0].latitude)
            intent.putExtra("longitude1", selected[0].longitude)

            intent.putExtra("name2", selected[1].name)
            intent.putExtra("latitude2", selected[1].latitude)
            intent.putExtra("longitude2", selected[1].longitude)
            startActivity(intent)
        }
    }

    override fun showSearchPlaceLoader() {
        places_search_progress.visibility = VISIBLE
    }

    override fun hidePlaceSearchLoader() {
        places_search_progress.visibility = INVISIBLE
    }

}

