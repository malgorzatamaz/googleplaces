package test.goggleplaces.core.places.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Candidate {
    fun getLongitude(): Double? {
        if (this.geometry != null && this.geometry!!.location != null) {
            return this.geometry!!.location!!.longitude!!
        }

        return null
    }

    fun getLatitude(): Double? {
        if (this.geometry != null && this.geometry!!.location != null) {
            return this.geometry!!.location!!.latitude!!
        }

        return null
    }

    @SerializedName("place_id")
    var id: String? = null

    @SerializedName("formatted_address")
    @Expose
    var address: String? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("geometry")
    var geometry: Geometry? = null

    class Geometry {
        @SerializedName("location")
        var location: Location? = null
    }

    class Location {
        @SerializedName("lat")
        var latitude: Double? = null

        @SerializedName("lng")
        var longitude: Double? = null
    }
}

