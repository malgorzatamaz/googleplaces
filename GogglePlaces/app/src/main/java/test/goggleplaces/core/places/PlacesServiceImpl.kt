package test.goggleplaces.core.places

import android.content.Context
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import test.goggleplaces.R
import test.goggleplaces.core.model.Place
import test.goggleplaces.core.places.listeners.DirectionsLoadedListener
import test.goggleplaces.core.places.listeners.FindPlaceListener
import test.goggleplaces.core.places.listeners.OnAutocompleteHintListener
import test.goggleplaces.core.places.model.Candidate
import test.goggleplaces.core.places.model.DirectionsResult
import test.goggleplaces.core.places.model.PlaceResult
import test.goggleplaces.core.places.model.PlacesResult
import test.goggleplaces.network.NetworkService
import test.goggleplaces.network.RetrofitClient
import javax.inject.Inject

class PlacesServiceImpl : PlacesService {
    private var googleService: NetworkService? = null
    private val baseUrl = "https://maps.googleapis.com"
    private var apiKey = ""
    private val RESULT_EMPTY = "ZERO_RESULTS"
    private val RESULT_OK = "OK"
    private val INPUT_TEXT = "textquery"

    @Inject
    constructor(context: Context) {
        apiKey = context.getString(R.string.api_key)
        googleService = getGoogleService()
    }

    private fun getGoogleService(): NetworkService {
        return RetrofitClient.getClient(baseUrl)!!.create<NetworkService>(NetworkService::class.java)
    }

    override fun getAutocompleteResult(value: String, listener: OnAutocompleteHintListener) {
        if (googleService != null)
            googleService!!.getPlacesAutocomplete(apiKey, value)
                    .subscribeOn(Schedulers.newThread())
                    ?.observeOn(AndroidSchedulers.mainThread())
                    ?.subscribe(object : DisposableObserver<PlacesResult>() {
                        override fun onComplete() {

                        }

                        override fun onNext(t: PlacesResult) {
                            if (t.status.equals(RESULT_EMPTY)) {
                                listener.onNotFound()
                            }

                            if (t.status.equals(RESULT_OK)) {

                                if (t.predictions != null)
                                    listener.onFound(
                                            t.predictions!!)
                            }
                        }

                        override fun onError(e: Throwable) {
                            listener.onError(e)
                        }
                    })
    }

    override fun findPlace(value: String, listener: FindPlaceListener) {
        if (googleService != null)
            googleService!!.getPlaceByName(apiKey, value, INPUT_TEXT)
                    .subscribeOn(Schedulers.newThread())
                    ?.observeOn(AndroidSchedulers.mainThread())
                    ?.subscribe(object : DisposableObserver<PlacesResult>() {
                        override fun onComplete() {

                        }

                        override fun onNext(t: PlacesResult) {
                            if (t.status.equals(RESULT_EMPTY)) {
                                listener.onNotFound()
                            }
                            if (t.status.equals(RESULT_OK)) {
                                if (t.candidates != null && t.candidates!!.isNotEmpty()) {
                                    val candidate = t.candidates!![0]

                                    if (candidate.name != null && candidate.getLatitude() != null && candidate.getLongitude() != null)
                                        listener.onFound(Place(candidate.name, candidate.address, candidate.getLatitude(), candidate.getLongitude()))
                                    else if (candidate.id != null)
                                        getPlaceById(candidate.id!!, listener)
                                    else
                                        listener.onNotFound()

                                }
                            } else {
                                listener.onNotFound()
                            }
                        }

                        override fun onError(e: Throwable) {
                            listener.onError(e)
                        }

                    })
    }

    override fun getPlaceById(id: String, listener: FindPlaceListener) {
        if (googleService != null)
            googleService!!.getPlaceById(apiKey, id)
                    .subscribeOn(Schedulers.newThread())
                    ?.observeOn(AndroidSchedulers.mainThread())
                    ?.subscribe(object : DisposableObserver<PlaceResult>() {
                        override fun onComplete() {

                        }

                        override fun onNext(result: PlaceResult) {
                            if (result.status.equals(RESULT_OK)) {
                                if (result.candidate != null) {
                                    var candidate = result.candidate
                                    listener.onFound(Place(candidate!!.name, candidate.address,
                                            candidate.getLatitude(), candidate.getLongitude()))
                                }
                            }
                            else{
                                listener.onNotFound()
                            }
                        }

                        override fun onError(e: Throwable) {
                            listener.onError(e)
                        }

                    })
    }

    override fun getDirections(lat: Double, lng: Double, lat2: Double, lng2: Double, directionsLoadedListener: DirectionsLoadedListener) {
        val origin = StringBuilder()
        val destination = StringBuilder()

        origin.append(java.lang.Double.toString(lat))
        origin.append(",")
        origin.append(java.lang.Double.toString(lng))

        destination.append(java.lang.Double.toString(lat2))
        destination.append(",")
        destination.append(java.lang.Double.toString(lng2))


        if (googleService != null)
            googleService!!.getDirections(apiKey, origin.toString(), destination.toString())
                    .subscribeOn(Schedulers.newThread())
                    ?.observeOn(AndroidSchedulers.mainThread())
                    ?.subscribe(object : DisposableObserver<DirectionsResult>() {
                        override fun onComplete() {

                        }

                        override fun onNext(result: DirectionsResult) {
                            directionsLoadedListener.onLoaded(result)
                        }

                        override fun onError(error: Throwable) {
                            directionsLoadedListener.onError(error)
                        }

                    })
    }
}