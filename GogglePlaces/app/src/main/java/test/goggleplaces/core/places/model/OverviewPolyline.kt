package test.goggleplaces.core.places.model

import com.google.gson.annotations.SerializedName

class OverviewPolyline {
    @SerializedName("points")
    var points = ""
}