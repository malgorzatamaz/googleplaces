package test.goggleplaces.core.presenters

import android.app.Activity
import test.goggleplaces.core.activities.MainActivity
import test.goggleplaces.core.model.Place
import java.lang.ref.WeakReference

interface MainPresenter{
    fun setMainView(mainView: WeakReference<MainView>)
    fun getSavedPlaces()
    fun addNewPlace()
    fun changeSelection(place: Place): Boolean
    fun getPlacesHint(value: CharSequence)
    fun showOnMap()
    fun deleteSelected()
    fun showRoute()
    fun searchPlace(placeName: String)
    fun requestTimePassed(): Boolean
    fun hideKeyboard(activity: Activity)
}