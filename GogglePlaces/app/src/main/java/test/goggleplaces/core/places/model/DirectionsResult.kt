package test.goggleplaces.core.places.model

import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class DirectionsResult {
    @SerializedName("status")
    var status = ""

    @SerializedName("routes")
    @Expose
    var routes: List<Route>? = null
}