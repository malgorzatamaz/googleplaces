package test.goggleplaces.core.places.listeners

import test.goggleplaces.core.model.Place

interface FindPlaceListener{
    fun onFound(place: Place)
    fun onNotFound()
    fun onError(error: Throwable)
}