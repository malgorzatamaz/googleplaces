package test.goggleplaces.core.adapters

import android.content.Context
import android.support.v4.content.ContextCompat
import android.widget.ArrayAdapter
import test.goggleplaces.core.model.Place
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import test.goggleplaces.R

import kotlinx.android.synthetic.main.place_row.view.*

class PlacesListAdapter(context: Context?, objects: List<Place>) :
        ArrayAdapter<Place>(context, 0, objects) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view: View? = null
        val place = getItem(position)

        if (convertView == null) {
            view = LayoutInflater.from(context).inflate(R.layout.place_row, parent, false)
        } else {
            view = convertView
        }

        if (!place.selected)
            view!!.setBackgroundColor(ContextCompat.getColor(view.context,android.R.color.white))

        view!!.google_place_name.text = place.name
        view.google_place_address.text = place.address

        return view
    }

    fun updateData(places: List<Place>) {
        this.clear()
        this.addAll(places)
    }
}