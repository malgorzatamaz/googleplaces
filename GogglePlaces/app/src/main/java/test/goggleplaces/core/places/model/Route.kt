package test.goggleplaces.core.places.model

import com.google.gson.annotations.SerializedName

class Route {
    @SerializedName("overview_polyline")
    var polyline: OverviewPolyline? = null
}