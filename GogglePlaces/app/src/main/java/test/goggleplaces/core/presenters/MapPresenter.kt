package test.goggleplaces.core.presenters

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import java.lang.ref.WeakReference
import java.util.ArrayList

interface MapPresenter{
    fun setMainView(mapView: WeakReference<MapView>)
    fun loadDirections(lat: Double, lng: Double, lat2: Double, lng2: Double)
    fun getRouteBounds(positions: ArrayList<LatLng>): LatLngBounds
    fun getBoundsZoomLevel(bounds: LatLngBounds, width: Int, height: Int): Int
}