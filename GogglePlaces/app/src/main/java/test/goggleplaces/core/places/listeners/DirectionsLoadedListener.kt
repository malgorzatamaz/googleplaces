package test.goggleplaces.core.places.listeners

import test.goggleplaces.core.places.model.DirectionsResult

interface DirectionsLoadedListener{
    fun onError(error: Throwable)
    fun onLoaded(result: DirectionsResult)
}