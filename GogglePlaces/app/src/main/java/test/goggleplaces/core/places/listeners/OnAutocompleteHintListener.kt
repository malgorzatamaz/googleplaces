package test.goggleplaces.core.places.listeners

import test.goggleplaces.core.places.model.Prediction

interface OnAutocompleteHintListener{
    fun onFound(foundHints: List<Prediction>)
    fun onNotFound()
    fun onError(error: Throwable)
}