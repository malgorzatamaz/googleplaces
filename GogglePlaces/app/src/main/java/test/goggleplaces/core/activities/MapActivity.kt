package test.goggleplaces.core.activities

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View.GONE
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import test.goggleplaces.R
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.GoogleMapOptions
import kotlinx.android.synthetic.main.activity_map.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.*
import test.goggleplaces.application.PlacesApplication
import test.goggleplaces.core.presenters.MapPresenter
import java.lang.ref.WeakReference
import java.util.ArrayList
import javax.inject.Inject
import android.content.Intent
import android.view.MenuItem


class MapActivity : AppCompatActivity(), test.goggleplaces.core.presenters.MapView, OnMapReadyCallback {
    private var mapView: MapView? = null

    private var map: GoogleMap? = null

    private var name: String? = ""

    private var lat: Double? = null

    private var lng: Double? = null

    private var name2: String? = ""

    private var lat2: Double? = null

    private var lng2: Double? = null


    @Inject
    lateinit var presenter: MapPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (application as PlacesApplication).mainComponent.inject(this)
        presenter.setMainView(WeakReference(this))

        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true);

        //TODO: serialize data!!!
        if (intent.hasExtra("name")) {
            name = intent.extras.getString("name")
            lat = intent.extras.getDouble("latitude")
            lng = intent.extras.getDouble("longitude")
        }

        if (intent.hasExtra("name1")) {
            name = intent.extras.getString("name1")
            lat = intent.extras.getDouble("latitude1")
            lng = intent.extras.getDouble("longitude1")

            name2 = intent.extras.getString("name2")
            lat2 = intent.extras.getDouble("latitude2")
            lng2 = intent.extras.getDouble("longitude2")
        }

        setContentView(R.layout.activity_map)

        val options = GoogleMapOptions()
        options.camera(CameraPosition.fromLatLngZoom(LatLng(51.0, 0.0), 4f))

        mapView = MapView(this, options)
        mapView!!.layoutParams = RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        map_container.addView(mapView, 0)
        mapView!!.onCreate(null)
        mapView!!.onResume()
        mapView!!.getMapAsync(this)
    }

    override fun onDirectionsSearchError() {
        showDialog(getString(R.string.places_alert_title),
                getString(R.string.direction_search_alert_message))

        loader.visibility = GONE
    }

    private fun showDialog(title: String, message: String) {
        val alertDialog = AlertDialog.Builder(this).create()
        alertDialog.setTitle(title)
        alertDialog.setMessage(message)
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK"
        ) { dialog, which -> dialog.dismiss() }
        alertDialog.show()
    }


    override fun onDirectionsLoaded(polyline: PolylineOptions) {
        if(map != null)
            map!!.addPolyline(polyline)

        loader.visibility = GONE
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        map = googleMap

        if (lat2 != null && lng2 != null) {
            val position = MarkerOptions()
                    .position(LatLng(lat!!, lng!!))
                    .title(name)

            map!!.addMarker(position)

            val position2 = MarkerOptions()
                    .position(LatLng(lat2!!, lng2!!))
                    .title(name2)

            map!!.addMarker(position2)

            val positions = ArrayList<LatLng>()
            positions.add(LatLng(lat!!, lng!!))
            positions.add(LatLng(lat2!!, lng2!!))

            zoomToPositions(positions)

            presenter.loadDirections(lat!!, lng!!, lat2!!, lng2!!)
        } else if (lat != null && lng != null) {
            loader.visibility = GONE

            val position = MarkerOptions()
                    .position(LatLng(lat!!, lng!!))
                    .title(name)

            val positions = ArrayList<LatLng>()
            positions.add(LatLng(lat!!, lng!!))

            map!!.addMarker(position)
            zoomToPositions(positions)
        }
    }

    private fun zoomToPositions(positions: ArrayList<LatLng>) {
        val bounds = presenter.getRouteBounds(positions)

        var zoom = presenter.getBoundsZoomLevel(bounds, mapView!!.measuredWidth, mapView!!.measuredHeight).toFloat()
       zoom = zoom * 90/100

        val cp = CameraPosition.Builder()
                .target(bounds.center)
                .zoom(zoom)
                .build()
        val cu = CameraUpdateFactory.newCameraPosition(cp)
        map!!.animateCamera(cu, 500, null)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()

        if (mapView != null)
            mapView!!.onResume()
    }

    override fun onPause() {
        super.onPause()

        if (mapView != null)
            mapView!!.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()

        if (mapView != null)
            mapView!!.onDestroy()
    }
}
