package test.goggleplaces.core.dao

import io.realm.RealmObject
import test.goggleplaces.core.model.Place
import test.goggleplaces.utils.UUIDGenerator

open class PlaceDao: RealmObject {
    var id = ""
    var name = ""
    var address = ""
    var latitude: Double = Place.NO_VALUE
    var longitude: Double = Place.NO_VALUE

    constructor(){
        this.id = UUIDGenerator.generateId()
    }

    constructor(name: String, address:String, latitude: Double, longitude: Double){
        this.id = UUIDGenerator.generateId()
        this.name = name
        this.address = address
        this.latitude = latitude
        this.longitude = longitude
    }

    constructor(place: Place){
        this.id = place.id
        this.name = place.name
        this.address = place.address
        this.latitude = place.latitude
        this.longitude = place.longitude
    }
}