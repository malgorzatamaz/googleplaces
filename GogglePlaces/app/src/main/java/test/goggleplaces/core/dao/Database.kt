package test.goggleplaces.core.dao

import rx.Observable
import test.goggleplaces.core.model.Place

interface Database{
    fun addNewPlace(name: String, address: String, latitude: Double, longitude: Double, listener: PlaceAddedListener)
    fun getAllPlaces(listener: PlacesLoadedListener)
    fun clear()
    fun destroy()
    fun getPlaceById(id:String, listener: PlacesLoadedListener)
    fun deletePlaceById(id: String, listener: PlaceDeletedListener)
    fun deletePlaces(selected: List<Place>, listener: PlaceDeletedListener)
}