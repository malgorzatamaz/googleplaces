package test.goggleplaces.core.places.model

import com.google.gson.annotations.SerializedName

class Prediction {
    @SerializedName("description")
    var description: String? = null

    @SerializedName("id")
    var id: String? = null
}