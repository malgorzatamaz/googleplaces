package test.goggleplaces.core.presenters

import android.content.Context
import test.goggleplaces.core.model.Place
import test.goggleplaces.core.places.model.Prediction

interface MainView {
    fun getContext(): Context
    fun onDeleteEnabled()
    fun onNavigateEnabled()
    fun onShowOnMapEnabled()

    fun onPlacesLoadError()
    fun onPlaceAddError()
    fun onPlaceDeleteError()

    fun onSavedPlacesLoaded(places: List<Place>)
    fun onSelectionCountChanged(selectedCount: Int)
    fun onHintLoaded(places: List<Prediction>)
    fun onShowSelectedMarker(name: String, latitude: Double?, longitude: Double?)
    fun disableAddButton()
    fun onPlaceNotFound()
    fun enableAddButton()
    fun onShowRoute(selected: List<Place>)
    fun showSearchPlaceLoader()
    fun hidePlaceSearchLoader()
}