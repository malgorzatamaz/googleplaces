package test.goggleplaces.core.presenters

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.PolylineOptions
import test.goggleplaces.R
import test.goggleplaces.core.places.PlacesService
import test.goggleplaces.core.places.listeners.DirectionsLoadedListener
import test.goggleplaces.core.places.model.DirectionsResult
import java.lang.ref.WeakReference
import java.util.ArrayList
import javax.inject.Inject

class MapPresenterImpl : MapPresenter {
    override fun getBoundsZoomLevel(bounds: LatLngBounds, width: Int, height: Int): Int {
        val ZOOM_MAX = 21
        val WORLD_PX_HEIGHT = 256
        val WORLD_PX_WIDTH = 256

        val ne = bounds.northeast
        val sw = bounds.southwest

        val latFraction = (latRad(ne.latitude) - latRad(sw.latitude)) / Math.PI

        val lngDiff = ne.longitude - sw.longitude
        val lngFraction = (if (lngDiff < 0) lngDiff + 360 else lngDiff) / 360

        val latZoom = zoom(height, WORLD_PX_HEIGHT, latFraction)
        val lngZoom = zoom(width, WORLD_PX_WIDTH, lngFraction)

        val result = Math.min(latZoom.toInt(), lngZoom.toInt())
        return Math.min(result, ZOOM_MAX)
    }

    fun zoom(mapPx: Int, worldPx: Int, fraction: Double): Double {
        val LN2 = 0.6931471805599453
        return Math.floor(Math.log(mapPx.toDouble() / worldPx.toDouble() / fraction) / LN2)
    }

    fun latRad(lat: Double): Double {
        val sin = Math.sin(lat * Math.PI / 180)
        val radX2 = Math.log((1 + sin) / (1 - sin)) / 2
        return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2
    }

    private lateinit var mapView: WeakReference<MapView>
    private var placesService: PlacesService

    @Inject
    constructor(placesService: PlacesService) {
        this.placesService = placesService
    }


    override fun setMainView(mapView: WeakReference<MapView>) {
        this.mapView = mapView
    }

    private fun createPolyline(points: List<LatLng>): PolylineOptions{
        return PolylineOptions()
                .addAll(points)
                .width(5f)
                .color(R.color.colorAccent)
                .geodesic(true)
    }

    private fun getRoute(encoded: String): List<LatLng> {
        val poly = ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0

        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat

            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng

            val p = LatLng(lat.toDouble() / 1E5,
                    lng.toDouble() / 1E5)
            poly.add(p)
        }

        return poly
    }

    override fun getRouteBounds(positions: ArrayList<LatLng>): LatLngBounds {
        val builder = LatLngBounds.Builder()
        for (i in positions) {
            builder.include(i)
        }
        return builder.build()
    }

    override fun loadDirections(lat: Double, lng: Double, lat2: Double, lng2: Double) {
        placesService.getDirections(lat, lng, lat2, lng2, object : DirectionsLoadedListener {
            override fun onError(error: Throwable) {
                if (mapView.get() != null)
                    mapView.get()!!.onDirectionsSearchError()
            }

            override fun onLoaded(result: DirectionsResult) {
                if (mapView.get() != null) {
                    if (result.routes != null && !result.routes!!.isEmpty() && result.routes!![0].polyline != null) {

                        var points = getRoute(result.routes!![0].polyline!!.points)
                        mapView.get()!!.onDirectionsLoaded(createPolyline(points))
                    }
                }
            }

        })
    }
}