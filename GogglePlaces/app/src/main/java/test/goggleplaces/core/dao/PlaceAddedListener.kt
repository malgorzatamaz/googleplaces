package test.goggleplaces.core.dao

import test.goggleplaces.core.model.Place

interface PlaceAddedListener{
    fun onAdded(place: Place)
    fun onError(error: Throwable)
}