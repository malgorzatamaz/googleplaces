package test.goggleplaces.core.dao

interface PlaceDeletedListener {
    fun onDeleted()
    fun onError(error: Throwable)
}