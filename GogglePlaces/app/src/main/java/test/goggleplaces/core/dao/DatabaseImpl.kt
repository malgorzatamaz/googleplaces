package test.goggleplaces.core.dao

import io.realm.Realm
import rx.Observable
import rx.Observer
import test.goggleplaces.core.model.Place


class DatabaseImpl : Database {
    override fun deletePlaces(selected: List<Place>, listener: PlaceDeletedListener) {
        realm.beginTransaction()
        deletePlacesByIds(selected).subscribe(object : Observer<Boolean> {
            override fun onError(e: Throwable?) {
                if (e != null)
                    listener.onError(e)

                realm.cancelTransaction()
            }

            override fun onNext(result: Boolean?) {
                if (result != null) {
                    if (result)
                        listener.onDeleted()
                    else
                        listener.onError(Throwable("Error while deleting place"))
                }
            }

            override fun onCompleted() {
                realm.commitTransaction()
            }
        })
    }

    private fun deletePlacesByIds(selected: List<Place>): Observable<Boolean>
    {
        var toDelete = realm.where(PlaceDao::class.java)

        for (i in 0 until selected.size - 1) {
            toDelete = toDelete.equalTo("id", selected.get(i).id).or()
        }
        toDelete = toDelete.equalTo("id", selected.get(selected.size - 1).id)


        return Observable.just(
                toDelete.findAll()
        ).map { query -> query.deleteAllFromRealm()}
    }

    override fun deletePlaceById(id: String, listener: PlaceDeletedListener) {
        realm.beginTransaction()
        deletePlace(id).subscribe(object : Observer<Boolean> {
            override fun onError(e: Throwable?) {
                if (e != null)
                    listener.onError(e)

                realm.cancelTransaction()
            }

            override fun onNext(result: Boolean?) {
                if (result != null) {
                    if (result)
                        listener.onDeleted()
                    else
                        listener.onError(Throwable("Error while deleting place"))
                }
            }

            override fun onCompleted() {
                realm.commitTransaction()
            }
        })
    }

    val realm: Realm = Realm.getDefaultInstance()

    override fun destroy() {
        realm.close()
    }

    override fun getAllPlaces(listener: PlacesLoadedListener) {
        realm.beginTransaction()
        getPlaces().subscribe(object : Observer<List<Place>> {
            override fun onError(e: Throwable?) {
                if (e != null)
                    listener.onError(e)
                realm.cancelTransaction()
            }

            override fun onNext(places: List<Place>?) {
                if (places != null)
                    listener.onPlacesLoaded(places)
            }

            override fun onCompleted() {
                realm.commitTransaction()
            }
        })
    }

    override fun getPlaceById(id: String, listener: PlacesLoadedListener) {
        realm.beginTransaction()
        getPlace(id).subscribe(object : Observer<Place> {
            override fun onError(e: Throwable?) {
                if (e != null)
                    listener.onError(e)
                realm.cancelTransaction()
            }

            override fun onNext(place: Place?) {
                val places = ArrayList<Place>()
                if (place != null) {
                    places.add(place)
                }
                listener.onPlacesLoaded(places)
            }

            override fun onCompleted() {
                realm.commitTransaction()
            }
        })
    }

    override fun addNewPlace(name: String, address: String, latitude: Double, longitude: Double, listener: PlaceAddedListener) {
        realm.beginTransaction()
        addPlace(name, address, latitude, longitude).subscribe(
                object : Observer<Place> {
                    override fun onCompleted() {
                        realm.commitTransaction()
                    }

                    override fun onError(e: Throwable) {
                        listener.onError(e)
                        realm.cancelTransaction()
                    }

                    override fun onNext(place: Place) {
                        listener.onAdded(place)
                    }
                })
    }

    override fun clear() {
        realm.beginTransaction()
        realm.deleteAll()
        realm.commitTransaction()
    }

    private fun addPlace(name: String, address: String, latitude: Double, longitude: Double): Observable<Place> {
        val place = PlaceDao(name, address, latitude, longitude)
        return Observable.just(Place(realm.copyToRealm(place)))
    }

    private fun deletePlace(id: String): Observable<Boolean> {
        return Observable.just(
                realm.where(PlaceDao::class.java).equalTo("id", id).findAll()
        ).map { query -> query.deleteAllFromRealm() }
    }

    private fun getPlaces(): Observable<List<Place>> {
        return Observable.from(
                realm.where(PlaceDao::class.java).findAll()
        ).toList().map { list -> createPlaceList(list) }
    }

    private fun createPlaceList(list: List<PlaceDao>): List<Place>? {
        return Observable.from(list).map { dao -> Place(dao) }.toList().toBlocking().single()
    }

    private fun getPlace(id: String): Observable<Place> {
        return Observable.just(
                realm.where(PlaceDao::class.java).equalTo("id", id).findFirst()
        ).map { dao -> Place(dao) }
    }

}