package test.goggleplaces.core.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import kotlinx.android.synthetic.main.autocomplete_row.view.*
import test.goggleplaces.R
import test.goggleplaces.core.places.model.Prediction

class PlacesAutocompleteAdapter : ArrayAdapter<Prediction> {
    constructor(context: Context, places: List<Prediction>) : super(context, 0, places) {
    }

    constructor(context: Context) : super(context, 0) {
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view: View? = null

        view = convertView ?: LayoutInflater.from(context).inflate(R.layout.autocomplete_row, parent, false)

        view!!.value.text = this.getItem(position).description

        return view
    }

    fun updateData(places: List<Prediction>) {
        this.clear()
        this.addAll(places)

        if (places.isNotEmpty()) {
            notifyDataSetChanged()
        } else {
            notifyDataSetInvalidated()
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val filterResults = FilterResults()
                if (constraint != null) {
                    filterResults.values = this
                    filterResults.count = count
                }
                else{
                    filterResults.values = ArrayList<String>()
                    filterResults.count = 0
                }

                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged()
                } else {
                    notifyDataSetInvalidated()
                }
            }
        }
    }
}
