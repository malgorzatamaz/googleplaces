package test.goggleplaces.core.presenters

import com.google.android.gms.maps.model.PolylineOptions

interface MapView {
    fun onDirectionsSearchError()
    fun onDirectionsLoaded(polyline: PolylineOptions)
}