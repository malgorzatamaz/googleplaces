package test.goggleplaces.core.model

import com.google.android.gms.maps.model.LatLng
import test.goggleplaces.core.dao.PlaceDao
import test.goggleplaces.utils.UUIDGenerator
import java.util.function.DoubleBinaryOperator

class Place {
    companion object {
    val NO_VALUE = 1000.0
    }

    var id = ""
    var name = ""
    var address = ""
    var latitude: Double = NO_VALUE
    var longitude: Double = NO_VALUE
    var selected = false

    constructor(name: String?, address:String?, latitude: Double?, longitude: Double?){
        id = UUIDGenerator.generateId()

        if(name != null)
        this.name = name

        if(address != null)
        this.address = address


        if(latitude != null)
            this.latitude = latitude

        if(longitude != null)
            this.longitude = longitude
    }

    constructor(placeDao: PlaceDao?){
        if(placeDao != null) {
            id = placeDao.id
            this.name = placeDao.name
            this.address = placeDao.address

            if(latitude != null)
                this.latitude = placeDao.latitude!!

            if(longitude != null)
                this.longitude = placeDao.longitude!!
        }
    }
}