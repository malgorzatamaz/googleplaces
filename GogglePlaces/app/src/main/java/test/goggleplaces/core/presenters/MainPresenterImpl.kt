package test.goggleplaces.core.presenters

import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager
import test.goggleplaces.core.dao.Database
import test.goggleplaces.core.dao.PlaceAddedListener
import test.goggleplaces.core.dao.PlaceDeletedListener
import test.goggleplaces.core.dao.PlacesLoadedListener
import test.goggleplaces.core.model.Place
import test.goggleplaces.core.places.*
import test.goggleplaces.core.places.listeners.FindPlaceListener
import test.goggleplaces.core.places.listeners.OnAutocompleteHintListener
import test.goggleplaces.core.places.model.Prediction
import java.lang.ref.WeakReference
import javax.inject.Inject

class MainPresenterImpl : MainPresenter {
    private var lastHintRequest: Long = 0
    var placesService: PlacesService
    var database: Database
    private lateinit var mainView: WeakReference<MainView>
    var placesList: ArrayList<Place>

    @Inject
    constructor(database: Database, placesService: PlacesService) {
        this.database = database
        this.placesService = placesService
        this.placesList = ArrayList()
    }

    override fun setMainView(mainView: WeakReference<MainView>) {
        this.mainView = mainView
    }

    override fun getPlacesHint(value: CharSequence) {
        placesService.getAutocompleteResult(value.toString(), object : OnAutocompleteHintListener {
            override fun onFound(foundHints: List<Prediction>) {
                if (mainView.get() != null)
                    mainView.get()!!.onHintLoaded(foundHints)
            }

            override fun onNotFound() {
                if (mainView.get() != null)
                    mainView.get()!!.disableAddButton()
            }

            override fun onError(error: Throwable) {
                if (mainView.get() != null)
                    mainView.get()!!.disableAddButton()
            }

        })
    }

    override fun changeSelection(place: Place): Boolean {
        val index = placesList.indexOf(place)
        placesList[index].selected = !placesList[index].selected

        var selectedCount = placesList.count { place -> place.selected }

        if (mainView.get() != null) {
            mainView.get()!!.onSelectionCountChanged(selectedCount)
        }

        return placesList[index].selected
    }

    override fun addNewPlace() {
        if (foundPlace == null)
            return

        database.addNewPlace(
                foundPlace!!.name,
                foundPlace!!.address,
                foundPlace!!.latitude,
                foundPlace!!.longitude,
                object : PlaceAddedListener {
                    override fun onError(error: Throwable) {
                        if (mainView.get() != null)
                            mainView.get()!!.onPlaceAddError()
                    }

                    override fun onAdded(place: Place) {
                        placesList.add(place)

                        if (mainView.get() != null)
                            mainView.get()!!.onSavedPlacesLoaded(placesList)
                    }
                })
    }

    override fun getSavedPlaces() {
        database.getAllPlaces(object : PlacesLoadedListener {
            override fun onPlacesLoaded(places: List<Place>) {
                if (mainView.get() != null)
                    mainView.get()!!.onSavedPlacesLoaded(places)

                placesList.clear()
                placesList.addAll(places)
            }

            override fun onError(error: Throwable) {
                if (mainView.get() != null)
                    mainView.get()!!.onPlacesLoadError()
            }
        })
    }

    private var foundPlace: Place? = null

    override fun searchPlace(placeName: String) {
        if (mainView.get() != null)
            mainView.get()!!.showSearchPlaceLoader()

        placesService.findPlace(placeName, object : FindPlaceListener {
            override fun onFound(place: Place) {
                if (mainView.get() != null && place != null)
                    mainView.get()!!.enableAddButton()

                foundPlace = place

                if (mainView.get() != null)
                    mainView.get()!!.hidePlaceSearchLoader()
            }

            override fun onNotFound() {
                if (mainView.get() != null)
                    mainView.get()!!.onPlaceNotFound()
            }

            override fun onError(error: Throwable) {
                if (mainView.get() != null)
                    mainView.get()!!.onPlaceNotFound()
            }
        })
    }

    override fun showOnMap() {
        val selected = placesList.first { place -> place.selected }
        if (mainView.get() != null)
            mainView.get()!!.onShowSelectedMarker(selected.name, selected.latitude, selected.longitude)
    }

    override fun deleteSelected() {
        val selected = placesList.filter { place -> place.selected }
        database.deletePlaces(selected, object : PlaceDeletedListener {
            override fun onDeleted() {
                placesList.removeAll(selected)
                placesList.forEach { place->place.selected = false }

                if (mainView.get() != null)
                    mainView.get()!!.onSavedPlacesLoaded(placesList)
            }

            override fun onError(error: Throwable) {
                if (mainView.get() != null)
                    mainView.get()!!.onPlaceDeleteError()
            }

        })
    }

    override fun showRoute() { val selected = placesList.filter { place -> place.selected }
        if (mainView.get() != null)
            mainView.get()!!.onShowRoute(selected)
    }

    override fun requestTimePassed(): Boolean {
        if(System.currentTimeMillis() - lastHintRequest > 1000){
            lastHintRequest = System.currentTimeMillis()
            return true
        }

        return false
    }

    override fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = activity.currentFocus
        if (view == null) {
            view = View(activity)
        }

        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}