package test.goggleplaces.core.places.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PlaceResult{
    @SerializedName("status")
    var status =""

    @SerializedName("result")
    @Expose
    var candidate: Candidate? = null
}