package test.goggleplaces.utils

import java.util.*

class UUIDGenerator{
    companion object {
        fun generateId(): String{
            return UUID.randomUUID().toString()
        }
    }
}